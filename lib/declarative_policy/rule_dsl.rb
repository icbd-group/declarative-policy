# frozen_string_literal: true

module DeclarativePolicy
  # The DSL evaluation context inside rule { ... } blocks.
  # Responsible for creating and combining Rule objects.
  #
  # See Base.rule
  #
  # ```
  #       def rule(&block)
  #         rule = RuleDsl.new(self).instance_eval(&block)
  #         PolicyDsl.new(self, rule)
  #       end
  # ```
  #
  class RuleDsl
    def initialize(context_class)
      @context_class = context_class
    end

    def can?(ability)
      Rule::Ability.new(ability)
    end

    # Usage:
    #
    # ```
    #   rule { all?(cond1, ~cond2) }.enable :ability1
    #   rule { all?(cond1, negate(cond2)) }.enable :ability1
    # ```
    def all?(*rules)
      Rule::And.make(rules)
    end

    def any?(*rules)
      Rule::Or.make(rules)
    end

    def none?(*rules)
      ~Rule::Or.new(rules)
    end

    def cond(condition)
      Rule::Condition.new(condition)
    end

    def delegate(delegate_name, condition)
      Rule::DelegatedCondition.new(delegate_name, condition)
    end

    def method_missing(msg, *args)
      return super unless args.empty? && !block_given?

      if @context_class.delegations.key?(msg)
        DelegateDsl.new(self, msg)
      else
        cond(msg.to_sym)
      end
    end

    def respond_to_missing?(symbol, include_all)
      true
    end
  end
end
