# frozen_string_literal: true

# Default policy definition for nil values
module DeclarativePolicy
  class NilPolicy < DeclarativePolicy::Base
    rule { default }.prevent_all
  end
end

# Built-in condition in Base:
#
# ```
#     desc 'Unknown user'
#     condition(:anonymous, scope: :user, score: 0) { @user.nil? }
#
#     desc 'By default'
#     condition(:default, scope: :global, score: 0) { true }
# ```
