# frozen_string_literal: true

module DeclarativePolicy
  # Will be extended to DeclarativePolicy.
  #
  # Thread safety through Thread.current variables.
  #
  # Usage: https://gitlab.com/gitlab-org/ruby/gems/declarative-policy/blob/main/doc/caching.md#preferred-scope
  #
  # When we set preferred_scope, this reduces the default score for conditions in
  # that scope, so that they are more likely to be executed first.
  #
  # - Conditions we have already cached get a score of 0.
  # - Conditions that are in the :global scope get a score of 2.
  # - **Conditions that are in the :user or :subject scopes get a score of 8.**
  # - Conditions that are in the :normal scope get a score of 16.
  #
  module PreferredScope
    PREFERRED_SCOPE_KEY = :"DeclarativePolicy.preferred_scope"

    def with_preferred_scope(scope)
      old_scope = Thread.current[PREFERRED_SCOPE_KEY]
      Thread.current[PREFERRED_SCOPE_KEY] = scope
      yield
    ensure
      Thread.current[PREFERRED_SCOPE_KEY] = old_scope
    end

    def preferred_scope
      Thread.current[PREFERRED_SCOPE_KEY]
    end

    def user_scope(&block)
      with_preferred_scope(:user, &block)
    end

    def subject_scope(&block)
      with_preferred_scope(:subject, &block)
    end

    def preferred_scope=(scope)
      Thread.current[PREFERRED_SCOPE_KEY] = scope
    end
  end
end
