# frozen_string_literal: true

module DeclarativePolicy
  class Configuration
    ConfigurationError = Class.new(StandardError)

    def initialize
      @named_policies = {}

      # Use lambda as a simpler method.
      #
      # User => UserPolicy
      #
      @name_transformation = ->(name) { "#{name}Policy" }

      @class_for = ->(name) { Object.const_get(name) }
    end

    # Combines the getter and setter.
    #
    # getter: `named_policy(:global)`
    # setter: `named_policy(:global, NewGlobalPolicy)`
    #
    def named_policy(name, policy = nil)
      @named_policies[name] = policy if policy

      @named_policies[name] || raise(ConfigurationError, "No #{name} policy configured")
    end

    # There's a built-in `NilPolicy` to prevent all.
    #
    def nil_policy(policy = nil)
      @nil_policy = policy if policy

      @nil_policy || ::DeclarativePolicy::NilPolicy
    end

    # Notice the parameter: `&block`
    #
    # Usage:
    #
    # ```
    #   DeclarativePolicy.configure do
    #     name_transformation { |name| "Policies::#{name}" }
    #   end
    # ```
    #
    def name_transformation(&block)
      @name_transformation = block
      nil
    end

    # Usage:
    #
    # ```
    #   DeclarativePolicy.configure do
    #     class_for do |name|
    #       (JH.const_defined?(name, false) && JH.const_get(name, false)) || \
    #         Object.const_get(name)
    #     end
    #   end
    # ```
    #
    def class_for(&block)
      @class_for = block
      nil
    end

    def policy_class(domain_class_name)
      return unless domain_class_name

      # Both `@class_for` and `@name_transformation` are lambda, so `call`.
      #
      # Flatten with default config:
      #  `Object.const_get("#{name}Policy")`
      #
      @class_for.call((@name_transformation.call(domain_class_name)))
    rescue NameError
      nil
    end
  end
end
