# frozen_string_literal: true

# rubocop: disable Lint/ConstantDefinitionInBlock
# rubocop: disable RSpec/LeakyConstantDeclaration
# rubocop: disable RSpec/PredicateMatcher
# rubocop: disable Style/BlockDelimiters
RSpec.describe 'Debug' do
  Car = Class.new do
    attr_reader :code

    def initialize(code)
      @code = code
    end
  end

  Driver = Class.new do
    attr_reader :age, :cars

    def initialize(age)
      @age = age
      @cars = []
    end

    def register(car)
      @cars << car
    end
  end

  CarPolicy = Class.new(DeclarativePolicy::Base) do
    condition(:own) {
      @user.cars.include?(@subject)
    }
    condition(:too_young) {
      @user.age < 18
    }

    rule { own }.enable :drive
    rule { too_young }.prevent :drive
    # rule { own & ~too_young }.enable :drive
  end

  context 'when allows' do
    let(:user) { Driver.new(20) }
    let(:car) { Car.new('沪A00001') }

    subject(:policy) { DeclarativePolicy.policy_for(user, car) }

    it 'allows drive' do
      user.register(car)

      expect(policy.allowed?(:drive)).to be_truthy
    end
  end
end
# rubocop: enable Lint/ConstantDefinitionInBlock
# rubocop: enable RSpec/LeakyConstantDeclaration
# rubocop: enable RSpec/PredicateMatcher
# rubocop: enable Style/BlockDelimiters
